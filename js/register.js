function featureDetection() {

    if ( ( 'serviceWorker' in navigator ) === false ) {

        return false;

    }

    return true;

}

async function registerSW() {

    try {

        await navigator.serviceWorker.register('./sw.js');

    } catch ( e ) {

        console.warn('ServiceWorker registration failed. Sorry about that.');

    }

}





if ( featureDetection() ) {

    window.addEventListener('load', e => {

        registerSW();

    });

}
