function deviceDelimeter() {

    var ua = navigator.userAgent;
    var os;
    var version;

    var uaIndex;

    if ( ua.match( /iPad/i ) || ua.match( /iPhone/i ) ) {

        os          = 'iOS';
        uaIndex     = ua.indexOf('OS ');

    } else if ( ua.match( /Android/i ) ) {

        os          = 'Android';
        uaIndex     = ua.indexOf('Android ');

    } else {

        os = null;

    }

    if ( os === 'iOS' && uaIndex > -1 ) {

        version = ua.substr( uaIndex + 3, 3 ).replace( '_', '.' );

    } else if (os === 'Android' && uaIndex > -1 ) {

        os = ua.substr(uaIndex + 8, 3);

    } else {

        version = null;

    }

    // console.log( ua );
    // console.log( os );
    // console.log( version );
    // console.log( uaIndex );

    if ( os === 'iOS' ) {

        if ( parseFloat( version ) <= 8 ) {

            return ';';

        } else {

            return '&';

        }

    } else {

        return '?';

    }

}

function formData() {

    var fullname = localStorage.getItem( 'fullname' );

    var address = localStorage.getItem( 'address' );

    if ( fullname === null || fullname === '' ) {

        return false;

    }

    if ( address === null || address === '' ) {

        return false;

    }

    return fullname + ' ' + address;

}

function formBody( occasion ) {

    var data = formData();

    if ( data === false ) {

        return false;

    }

    return encodeURIComponent( occasion + ' ' + data );

}

function formSms( occasion ) {

    var body = formBody( occasion );

    if ( body === false ) {

        return '/settings.html';

    }

    return 'sms:+3013033' + deviceDelimeter() + 'body=' + formBody( occasion );

}
