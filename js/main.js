function click_btn( event ) {

    if ( event.currentTarget.getAttribute( 'href' ) === '/settings.html' ) {

        localStorage.setItem( 'pendingmsg', event.currentTarget.getAttribute( 'data-occasion' ) );

    }

}

function bind_buttons() {

    var buttons = document.querySelectorAll( '.options a' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].addEventListener( 'click', click_btn );

        buttons[ i ].setAttribute( 'href', formSms( buttons[ i ].getAttribute( 'data-occasion' ) ) );

    }

}

formData();

bind_buttons();
