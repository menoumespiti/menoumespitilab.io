function submit_data( event ) {

    event.preventDefault();

    localStorage.setItem( 'fullname', document.getElementById( 'fullname' ).value );

    localStorage.setItem( 'address', document.getElementById( 'address' ).value );

    if ( thereis_pendingMsg() ) {

        var pendingmsg = localStorage.getItem( 'pendingmsg' );

        deletePendingMsg();

        window.location.href = formSms( pendingmsg );

    } else {

        window.location.href = "/";

    }

}

function populateData() {

    var fullname = localStorage.getItem( 'fullname' );

    var address = localStorage.getItem( 'address' );

    if ( fullname !== null && fullname !== '' ) {

        document.getElementById( 'fullname' ).value = fullname;

    }

    if ( address !== null && address !== '' ) {

        document.getElementById( 'address' ).value = address;

    }

}

function thereis_pendingMsg() {

    var pending = localStorage.getItem( 'pendingmsg' );

    if ( pending === null ) {

        return false;

    } else {

        return true;

    }

}

function deletePendingMsg() {

    localStorage.removeItem( 'pendingmsg' );

}

populateData();

if ( thereis_pendingMsg() ) {

    document.getElementById( 'firsttime' ).style.display = 'block';

}

document.getElementById( 'data' ).addEventListener( 'submit', submit_data );
