const cacheName = '13033sms';
const staticAssets = [
  './',
  './index.html',
  './settings.html',
  './about.html',
  './css/main.css',
  './js/global.js',
  './js/main.js',
  './js/register.js',
  './js/settings.js',
  './fonts/KFOmCnqEu92Fr1Mu4mxK.woff2',
  './fonts/KFOmCnqEu92Fr1Mu4WxKOzY.woff2',
  './manifest.json',
  './media/icon.svg',
  './media/icon-512.png',
  './media/icon-192.png',
  './contribute.html',
  './media/cctracker.png',
  './media/cleancode.png'
];

async function networkfirst(req) {

      const cache = await caches.open( cacheName );

      try {

            const fresh = await fetch( req );
            cache.put( req, fresh.clone() );
            return fresh;

      } catch (e) {

            const cachedResponse = await cache.match(req);
            return cachedResponse;

      }

}




self.addEventListener('install', async event => {

    const cache = await caches.open( cacheName );

    await cache.addAll( staticAssets );

});

self.addEventListener('fetch', async event => {

    const req = event.request;

    event.respondWith( networkfirst( req ) );

});
